const courseData = [
    {
        id: "wdc001",
        name: "PHP - Laravel",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat, consectetur.",
        price: 45000,
        onOffer: true
    }, {
        id: "wdc002",
        name: "Java - Springboot",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat, consectetur.",
        price: 50000,
        onOffer: true
    }, {
        id: "wdc003",
        name: "Node - Express",
        description: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Quaerat, consectetur.",
        price: 55000,
        onOffer: true
    }
]

export default courseData