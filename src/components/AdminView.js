import {useState, Fragment, useEffect} from "react";
import {
    Container,
    Button,
    Row,
    Col,
    Table,
    Modal,
    Form
} from "react-bootstrap";
import Swal from "sweetalert2";


export default function AdminView (props) {
    const {courseData, fetchData} = props

    const [name, setName] = useState ("")
    const [description, setDescription] = useState ("")
    const [price, setPrice] = useState (0)
    const [courseId, setCourseId] = useState("")
    // const [isActive, setIsActive] = useState(null)
    const [courses, setCourses] = useState([])
    const [showAdd, setShowAdd] = useState(false)

    //show modal function for add new course
    const openAdd = () => setShowAdd(true)
    const closeAdd = () => setShowAdd(false)


    useEffect( () => {
            const courseArr = courseData.map (course => {
                setCourseId(course._id)
                return (
                    <tr>
                        <td>{
                            course.courseName
                        }</td>
                        <td>{
                            course.description
                        }</td>
                        <td>{
                            course.price
                        }</td>
                        <td>{
                            (course.isActive === true) ? <span>Available</span> : <span>Unavailable</span>
                        } </td>
                        <td>
						<Button onClick={() => openEdit(course._id)}>Update</Button>
						{
							(course.isActive) ?
								<Fragment>
									<Button 
										variant="danger"
										onClick={ () => disableCourse(course._id, course.isActive)}
										>Disable</Button>
									<Button variant="secondary">Delete</Button>
								</Fragment>
							:
								<Fragment>
									<Button variant="success" onClick={ () => enableCourse(course._id, course.isActive)} >Enable</Button>
									<Button variant="secondary">Disable</Button>
								</Fragment>
								
						}
					</td>

                    </tr>
                )
            })

            setCourses(courseArr)
            
    }, [courseData])


    // Add new course function
    const addCourse = (e) => {
        e.preventDefault()

        fetch("http://localhost:3000/api/courses/create-course", {
            method:"POST",
            headers:{
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`,
            },
            body:JSON.stringify({
                courseName: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json()).then(data => {
            if(data === true) {
                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Created course successfully"
                })

                setName("");
                setDescription("");
                setPrice(0);
                closeAdd();
                fetchData();
            } else {
                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again"
                })

                fetchData();
            }
        })
    }

    // Auto-populate data in modal form
    const openEdit = (courseId) => {

        openAdd()
        fetch(`http://localhost:3000/api/courses/${courseId}`, {
            headers: {
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            }
        }).then(response => response.json()).then((data) => {
            setName(data.courseName);
            setDescription(data.description);
            setPrice(data.price);
        })
    }
    
    //Edit course modal
    const editCourse = (e, courseId) => {
        e.preventDefault()

        fetch(`http://localhost:3000/api/courses/${courseId}/edit`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`
            },
            body: JSON.stringify({
                courseName: name,
                description: description,
                price: price
            })
        })
        .then(response => response.json())
        .then(data => {
            // console.log(data) //object

            if(typeof data !== "undefined"){
                fetchData()

                Swal.fire({
                    title: "Success",
                    icon: "success",
                    text: "Course successfully updated."
                })

                closeAdd()
            } else {

                Swal.fire({
                    title: "Something went wrong",
                    icon: "error",
                    text: "Please try again."
                })

                fetchData()
            }
        })
    }


    //enable course
    const enableCourse = (courseId, isActive) => {
        fetch(`http://localhost:3000/api/courses/${courseId}/unarchive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                isActive:!isActive
            })
        }).then(response => response.json()).then(data => {
            if (data === true) {
                Swal.fire({
                    title:"Success",
                    icon:"success",
                    text: "Enabled course"
                })
                fetchData();
            } else {
                Swal.fire({
                    title:"Error",
                    icon:"error",
                    text:"There was an error"
                })
                fetchData();
            }
        })
    }

    //disable course
    const disableCourse = (courseId, isActive) => {
        fetch(`http://localhost:3000/api/courses/${courseId}/archive`, {
            method: "PUT",
            headers: {
                "Content-Type": "application/json",
                "Authorization": `Bearer ${localStorage.getItem("token")}`,
            },
            body: JSON.stringify({
                isActive:!isActive
            })
        }).then(response => response.json()).then(data => {
            if (data === true) {
                Swal.fire({
                    title:"Success",
                    icon:"success",
                    text: "Disabled course"
                })
                fetchData();
            } else {
                Swal.fire({
                    title:"Error",
                    icon:"error",
                    text:"There was an error"
                })
                fetchData();
            }
        })
    }

    return (
        <Container>
            <h2 className="text-center mt-5">Admin Dashboard</h2>
            <Row className="justify-content-center">
                <Col>
                    <div className="text-right">
                        <Button onClick={openAdd}>
                            Add New Course</Button>
                    </div>
                </Col>
            </Row>
            <Table className="mt-3" striped bordered hover>
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Description</th>
                        <th>Price</th>
                        <th>Status</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>
                    {courses}
                </tbody>
            </Table>

            {/* add course modal */}
            <Modal show={showAdd} onHide={closeAdd}>
                <Modal.Header closeButton>
                    <Modal.Title>Add New Course</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => addCourse(e)}>
                        <Form.Group className="m-2" controlID="courseName">
                            <Form.Label>Course Name</Form.Label>
                            <Form.Control type="text" value={name}onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group className="m-2" controlID="courseDescription">
                            <Form.Label>Course Description</Form.Label>
                            <Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="m-1" controlID="coursePrice">
                            <Form.Label>Course Price</Form.Label>
                            <Form.Control type="number" value={price} onChange={(e) => setPrice(e.target.value)}/>
                        </Form.Group>
                        <Button className="m-1"variant="success" type="submit">Submit</Button>
                        <Button className="m-2"variant="secondary" type="submit" onClick={closeAdd}>Close</Button>
                    </Form>
                </Modal.Body>
            </Modal>

            {/* edit course modal */}
            <Modal show={showAdd} onHide={closeAdd}>
                <Modal.Header closeButton>
                    <Modal.Title>Edit Course</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <Form onSubmit={(e) => editCourse(e, courseId)}>
                        <Form.Group className="m-2" controlID="courseName">
                            <Form.Label>Course Name</Form.Label>
                            <Form.Control type="text" value={name}onChange={(e) => setName(e.target.value)} />
                        </Form.Group>
                        <Form.Group className="m-2" controlID="courseDescription">
                            <Form.Label>Course Description</Form.Label>
                            <Form.Control type="text" value={description} onChange={(e) => setDescription(e.target.value)}/>
                        </Form.Group>
                        <Form.Group className="m-1" controlID="coursePrice">
                            <Form.Label>Course Price</Form.Label>
                            <Form.Control type="number" value={price} onChange={(e) => setPrice(e.target.value)}/>
                        </Form.Group>
                        <Button className="m-1"variant="success" type="submit">Submit</Button>
                        <Button className="m-2"variant="secondary" type="submit" onClick={closeAdd}>Close</Button>
                    </Form>
                </Modal.Body>
            </Modal>

        </Container>
    )
}
