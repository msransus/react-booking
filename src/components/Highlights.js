


export default function Highlights() {
    return(
        <div className="container-fluid">
            <div className="row mb-3">

                {/* Card 1 */}
                <div className="col-10 col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Learn from Home</h5>
                        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda temporibus veritatis explicabo accusamus quibusdam facere et recusandae hic totam minus.</p>
                    </div>
                    </div>
                </div>

                {/* Card 2 */}
                <div className="col-10 col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Study Now, Pay Later</h5>
                        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda temporibus veritatis explicabo accusamus quibusdam facere et recusandae hic totam minus.</p>
                    </div>
                    </div>
                </div>

                {/* Card 3 */}
                <div className="col-10 col-md-4">
                <div class="card">
                    <div class="card-body">
                        <h5 class="card-title">Be part of our community</h5>
                        <p class="card-text">Lorem, ipsum dolor sit amet consectetur adipisicing elit. Assumenda temporibus veritatis explicabo accusamus quibusdam facere et recusandae hic totam minus.</p>
                    </div>
                    </div>
                </div>
            </div>
        </div>
    )
}